# Description
Mission Critical is a simple arcade style game based on the movement and shooting mechanics of The Binding of Isaac.

![Gameplay](.images/action.png)
![Game Over](.images/gameover.png)

Controls:
- WASD to move
- Arrow keys to shoot
- Enter or Space to start a new game
- Escape to quit

Rules:
- Shoot an enemy three times to kill it
- Getting hit by an enemy shot will take away a life
- Lose 3 lives and the game is over
- Getting hit directly by an enemy will cause an instant game over

Additional Info:
- Every kill will cause enemies to spawn in slightly faster.
- Enemies will not spawn faster than 1 per second.
- Each time you lose a life, the enemy spawn rate is dramatically reduced.

# How to build
Mission Critical is written in Zig (https://ziglang.org/) and uses the zig build system.

You will need Zig version 0.11.0 in order to build.

To build:
```
git submodule update --init
git -C raylib-zig apply ../0001-Update-for-zig-master.patch
zig build -Doptimize=ReleaseFast
```

# License
This project and all files associated with it is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
