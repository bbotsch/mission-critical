const std = @import("std");
const rl = @import("raylib");
const Game = @import("Game.zig");
const Menu = @This();
const StringArray = std.BoundedArray(u8, 128);

state: State = .main,
hover: Hover = .none,
text: Text = .host,
host_string: StringArray = StringArray.init(0) catch unreachable,
port_string: StringArray = StringArray.init(0) catch unreachable,
lobby_thread: ?std.Thread = null,

const State = enum {
    main,
    host,
    join,
    accepting,
    waiting,
};

const Hover = enum {
    none,
    play,
    host,
    join,
    start,
};

const Text = enum {
    host,
    port,
};

const border_size = 10;

const play_button = rl.Rectangle.init(Game.screen_width / 2 - 50 - 20 - 100, Game.screen_height / 2 - 25, 100, 50);
const host_button = rl.Rectangle.init(Game.screen_width / 2 - 50, Game.screen_height / 2 - 25, 100, 50);
const join_button = rl.Rectangle.init(Game.screen_width / 2 + 50 + 20, Game.screen_height / 2 - 25, 100, 50);
const start_button = rl.Rectangle.init(Game.screen_width / 2 - 50, Game.screen_height / 2 + 25, 100, 50);

const host_box = rl.Rectangle.init(Game.screen_width / 2 - 100, Game.screen_height / 2 - 50 - 10, 200, 50);
const port_box = rl.Rectangle.init(Game.screen_width / 2 - 100, Game.screen_height / 2 + 10, 200, 50);

pub fn init() Menu {
    var result = Menu{};
    for (&result.host_string.buffer) |*byte| {
        byte.* = 0;
    }
    for (&result.port_string.buffer) |*byte| {
        byte.* = 0;
    }
    return result;
}

pub fn updateState(self: *Menu, game: *Game) !void {
    const mouse = rl.getMousePosition();
    const button_left_pressed = rl.isMouseButtonPressed(rl.MouseButton.mouse_button_left);

    switch (self.state) {
        .main => {
            if (self.lobby_thread) |thread| {
                thread.join();
                self.lobby_thread = null;
                game.state = .running;
            }

            if (rl.checkCollisionPointRec(mouse, play_button)) {
                self.hover = .play;
                if (button_left_pressed) {
                    game.state = .running;
                }
            } else if (rl.checkCollisionPointRec(mouse, host_button)) {
                self.hover = .host;
                if (button_left_pressed) {
                    self.state = .host;
                }
            } else if (rl.checkCollisionPointRec(mouse, join_button)) {
                self.hover = .join;
                if (button_left_pressed) {
                    self.state = .join;
                }
            } else {
                self.hover = .none;
            }
        },
        .accepting => {
            if (rl.checkCollisionPointRec(mouse, start_button)) {
                self.hover = .start;
                if (button_left_pressed) {
                    self.state = .main;
                }
            } else {
                self.hover = .none;
            }
        },
        .waiting => {}, // could implement a disconect button here
        inline .host, .join => |state| {
            if (rl.isKeyPressed(rl.KeyboardKey.key_enter) or rl.isKeyPressed(rl.KeyboardKey.key_tab)) {
                switch (self.text) {
                    .host => {
                        self.text = .port;
                    },
                    .port => {
                        const port = try std.fmt.parseInt(u16, self.port_string.slice(), 10);
                        self.lobby_thread = game.startLobby(.{
                            .connection_type = if (state == .host) .server else .client,
                            .host = self.host_string.slice(),
                            .port = port,
                        }) catch blk: {
                            game.state = .disconnected;
                            break :blk null;
                        };
                        self.state = if (state == .host) .accepting else .waiting;
                        return;
                    },
                }
            }

            const char_pressed = rl.getCharPressed();
            const codepoint: u21 = @intCast(@as(u32, @bitCast(char_pressed)));
            const codepoint_len = std.unicode.utf8CodepointSequenceLength(codepoint) catch return;
            if (char_pressed != 0) {
                const buffer = switch (self.text) {
                    .host => switch (codepoint_len) {
                        inline 1, 2, 3, 4 => |l| try self.host_string.addManyAsArray(l),
                        else => unreachable,
                    },
                    .port => switch (codepoint_len) {
                        inline 1, 2, 3, 4 => |l| try self.port_string.addManyAsArray(l),
                        else => unreachable,
                    },
                };
                _ = try std.unicode.utf8Encode(codepoint, buffer);
            }
        },
    }
}

pub fn draw(self: Menu) void {
    switch (self.state) {
        .main => {
            rl.drawRectangleRounded(play_button, 0, 0, rl.Color.green);
            rl.drawRectangleRounded(host_button, 0, 0, rl.Color.magenta);
            rl.drawRectangleRounded(join_button, 0, 0, rl.Color.gray);

            const play_color = if (self.hover == .play) rl.Color.gold else rl.Color.white;
            const host_color = if (self.hover == .host) rl.Color.gold else rl.Color.white;
            const join_color = if (self.hover == .join) rl.Color.gold else rl.Color.white;
            rl.drawText("Play", play_button.x + border_size, play_button.y + border_size, 24, play_color);
            rl.drawText("Host", host_button.x + border_size, host_button.y + border_size, 24, host_color);
            rl.drawText("Join", join_button.x + border_size, join_button.y + border_size, 24, join_color);
        },
        .host, .join => {
            rl.drawText("Host", host_box.x - 75, host_box.y + border_size, 24, rl.Color.black);
            rl.drawText("Port", port_box.x - 75, port_box.y + border_size, 24, rl.Color.black);

            rl.drawRectangleRounded(host_box, 0, 0, rl.Color.light_gray);
            rl.drawRectangleRounded(port_box, 0, 0, rl.Color.light_gray);

            const host_slice = self.host_string.buffer[0..self.host_string.len :0];
            const port_slice = self.port_string.buffer[0..self.port_string.len :0];
            rl.drawText(host_slice, host_box.x + border_size, host_box.y + border_size, 18, rl.Color.black);
            rl.drawText(port_slice, port_box.x + border_size, port_box.y + border_size, 18, rl.Color.black);
        },
        .accepting => {
            rl.drawText("Waiting for clients...", Game.screen_width / 2 - 200, Game.screen_height / 2 - 50, 48, rl.Color.blue);

            rl.drawRectangleRounded(start_button, 0, 0, rl.Color.green);
            const color = if (self.hover == .start) rl.Color.gold else rl.Color.white;
            rl.drawText("Start", start_button.x + border_size, start_button.y + border_size, 24, color);
        },
        .waiting => {
            rl.drawText("Connected", 5, 5, 24, rl.Color.dark_green);
            rl.drawText("Waiting for server...", Game.screen_width / 2 - 200, Game.screen_height / 2 - 50, 48, rl.Color.blue);
        },
    }
}
