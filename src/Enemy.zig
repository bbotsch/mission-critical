const std = @import("std");
const rl = @import("raylib");
const Context = @import("Game.zig");
const Bullet = @import("Bullet.zig");
const rmath = @import("raylib-math");
const Enemy = @This();

const multiplier = 1.5 * 60;

position: rl.Vector2 = rl.Vector2.init(0, 0),
speed: rl.Vector2 = rl.Vector2.init(0, 0),
max_speed: f32 = 2 * multiplier,
accel: rl.Vector2 = rl.Vector2.init(0, 0),
max_accel: f32 = 20 * multiplier,
friction: f32 = 20 * multiplier,
radius: f32 = 20,
bullet_delay: f32 = 0,
max_delay: f32 = 1.0 / 3.0,
shotspeed: f32 = 2 * multiplier,
range: f32 = 1,
hp: f32 = 10,

pub fn randomizeStats(self: *Enemy, prng: std.rand.Random) void {
    self.position.x += prng.float(f32) * Context.screen_width;
    self.position.y += prng.float(f32) * Context.screen_height;

    self.max_speed += ((prng.float(f32) * 4) - 2) * multiplier;
    self.max_accel += ((prng.float(f32) * 40) - 20) * multiplier;
    self.friction += ((prng.float(f32) * 40) - 20) * multiplier;
    self.radius += ((prng.float(f32) * 20) - 10) * multiplier / 120.0;
    self.max_delay = 1.0 / prng.float(f32);
    self.shotspeed += ((prng.float(f32) * 2) - 1) * multiplier;
    self.range += (prng.float(f32) - 0.5);
}

pub fn updateState(self: *Enemy, ctx: *Context, frametime: f32) !void {
    const players_slice = ctx.players.items;
    var closest_player = &players_slice[0];
    var shortest_distance = rmath.vector2Distance(self.position, players_slice[0].position);
    for (players_slice[1..]) |*player| {
        const distance = rmath.vector2Distance(self.position, player.position);
        if (distance < shortest_distance) {
            shortest_distance = distance;
            closest_player = player;
        }
    }

    const to_player = rmath.vector2Normalize(rmath.vector2Subtract(closest_player.position, self.position));

    self.accel = to_player;
    self.accel.x *= ctx.prng.float(f32);
    self.accel.y *= ctx.prng.float(f32);
    self.accel = rmath.vector2Scale(rmath.vector2Normalize(self.accel), self.max_accel);

    const fric = rmath.vector2Scale(rmath.vector2Normalize(self.speed), -self.friction);
    const new_speed = rmath.vector2Add(self.speed, rmath.vector2Scale(fric, frametime));
    self.speed.x = if (self.speed.x * new_speed.x > 0) new_speed.x else 0;
    self.speed.y = if (self.speed.y * new_speed.y > 0) new_speed.y else 0;

    self.speed = rmath.vector2Add(self.speed, rmath.vector2Scale(self.accel, frametime));
    self.speed = rmath.vector2ClampValue(self.speed, 0, self.max_speed);

    self.position = rmath.vector2Add(self.position, rmath.vector2Scale(self.speed, frametime));

    self.bullet_delay -= frametime;
    if (self.bullet_delay <= 0) {
        var bullet_speed = to_player;
        bullet_speed.x *= ctx.prng.float(f32);
        bullet_speed.y *= ctx.prng.float(f32);
        bullet_speed = rmath.vector2Scale(rmath.vector2Normalize(bullet_speed), self.shotspeed);

        try ctx.enemy_bullets.append(ctx.allocator, .{
            .position = self.position,
            .speed = bullet_speed,
            .range = self.range,
            .color = rl.Color.dark_blue,
        });
        self.bullet_delay += self.max_delay;
    }
}
