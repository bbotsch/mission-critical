// raylib-zig (c) Nikolas Wipper 2023

const std = @import("std");
const builtin = @import("builtin");
const rl = @import("raylib");
const Context = @import("Game.zig");

fn drawScreen(ctx: *Context) !void {
    rl.beginDrawing();
    defer rl.endDrawing();

    rl.clearBackground(rl.Color.ray_white);
    try ctx.draw();
    rl.drawFPS(Context.screen_width - 85, 5);
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){
        .backing_allocator = if (comptime builtin.target.isWasm()) std.heap.wasm_allocator else std.heap.page_allocator,
    };
    defer std.debug.assert(gpa.deinit() == .ok);
    const allocator = gpa.allocator();

    var ctx = try Context.init(allocator);
    defer ctx.deinit();
    defer ctx.connection_manager.deinit();

    // try ctx.startConnectionsPub();

    rl.initWindow(Context.screen_width, Context.screen_height, "Mission Critical");
    defer rl.closeWindow();

    ctx.settings.apply();

    rl.setExitKey(rl.KeyboardKey.key_null);
    while (!rl.windowShouldClose() and (ctx.state != .quit)) {
        const frametime = rl.getFrameTime();
        try ctx.updateState(frametime);
        try drawScreen(&ctx);
    }
}
