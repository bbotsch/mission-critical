const std = @import("std");
const builtin = @import("builtin");
const rl = @import("raylib");
const rmath = @import("raylib-math");
const Player = @import("Player.zig");
const Enemy = @import("Enemy.zig");
const Bullet = @import("Bullet.zig");
const ConnectionManager = @import("ConnectionManager.zig");
const Menu = @import("Menu.zig");
const Game = @This();

pub const screen_width = 1280;
pub const screen_height = 720;

allocator: std.mem.Allocator,
rwlock: std.Thread.RwLock = std.Thread.RwLock{},
state: State = .menu,
menu: Menu = Menu.init(),
settings: Settings = .{},
connection_manager: ConnectionManager,
prng: std.rand.Random,
seed: u64 = undefined,
players: std.ArrayListUnmanaged(Player),
player_id: u8 = undefined,
enemies: std.ArrayListUnmanaged(Enemy),
enemy_bullets: std.ArrayListUnmanaged(Bullet),
enemy_delay: f32 = 0,
min_enemy_delay: f32 = 0.8,
max_enemy_delay: f32 = 3,

pub const State = enum {
    menu,
    running,
    disconnected,
    paused,
    gameover,
    quit,
};

const Settings = struct {
    vsync: bool = true,

    pub fn apply(self: Settings) void {
        if (self.vsync) {
            rl.setTargetFPS(rl.getMonitorRefreshRate(rl.getCurrentMonitor()));
        } else {
            rl.setTargetFPS(0);
        }
    }
};

fn generateSeed() !u64 {
    var seed: u64 = undefined;
    if (comptime builtin.target.isWasm()) {
        const stamp = std.time.nanoTimestamp();
        const stamp_u128: u128 = @bitCast(stamp);
        seed = @truncate(stamp_u128);
    } else {
        try std.os.getrandom(std.mem.asBytes(&seed));
    }
    return seed;
}

pub fn init(allocator: std.mem.Allocator) !Game {
    return initSeed(
        allocator,
        try generateSeed(),
    );
}

pub fn initSeed(allocator: std.mem.Allocator, seed: u64) !Game {
    const players = [1]Player{try Player.init(allocator, null)};
    return initFull(
        allocator,
        players[0].player_id,
        &players,
        seed,
        std.Thread.RwLock{},
        try ConnectionManager.init(allocator, .local, .{}),
    );
}

fn initFull(
    allocator: std.mem.Allocator,
    player_id: u8,
    players: []const Player,
    seed: u64,
    rwlock: std.Thread.RwLock,
    connection_manager: ConnectionManager,
) !Game {
    var game = Game{
        .allocator = allocator,
        .rwlock = rwlock,
        .connection_manager = connection_manager,
        .seed = seed,
        .player_id = player_id,
        .players = try std.ArrayListUnmanaged(Player).initCapacity(allocator, players.len),
        .enemies = try std.ArrayListUnmanaged(Enemy).initCapacity(allocator, 0),
        .enemy_bullets = try std.ArrayListUnmanaged(Bullet).initCapacity(allocator, 0),
        .prng = blk: {
            const prng = try allocator.create(std.rand.DefaultPrng);
            prng.* = std.rand.DefaultPrng.init(seed);
            break :blk prng.random();
        },
    };

    const separation: f32 = screen_width / @as(f32, @floatFromInt(players.len + 1));
    for (0.., players) |i, player| {
        const i_f32: f32 = @floatFromInt(i);
        var new = try Player.init(
            allocator,
            player.player_id,
        );
        new.position = rl.Vector2.init(separation * (i_f32 + 1), screen_height / 2);
        game.players.appendAssumeCapacity(new);
    }

    return game;
}

// Need to change now reinitting works so it doesn't recreate from scratch, but rather reinitializes appropriate fields
fn reinit(self: *Game) !void {
    const new = try Game.initFull(
        self.allocator,
        self.player_id,
        self.players.items,
        try generateSeed(),
        self.rwlock,
        self.connection_manager,
    );

    self.deinit();
    self.* = new;
}

pub fn deinit(self: *Game) void {
    for (self.players.items) |*player| {
        player.deinit(self.allocator);
    }
    self.players.deinit(self.allocator);
    self.enemies.deinit(self.allocator);
    self.enemy_bullets.deinit(self.allocator);

    const prng: *std.rand.DefaultPrng = @ptrCast(@alignCast(self.prng.ptr));
    self.allocator.destroy(prng);
}

pub fn getPlayer(self: Game, player_id: u8) ?*Player {
    for (self.players.items) |*player| {
        if (player.player_id == player_id) return player;
    }
    return null;
}

fn addEnemy(self: *Game) !void {
    var enemy = Enemy{};
    enemy.randomizeStats(self.prng);

    for (self.players.items) |player| {
        const distance = rmath.vector2Distance(enemy.position, player.position);
        if (distance < 200) return;
    }

    try self.enemies.append(self.allocator, enemy);
    self.max_enemy_delay += 0.05;
}

inline fn touching(obj1: anytype, obj2: anytype) bool {
    return rl.checkCollisionCircles(obj1.position, obj1.radius, obj2.position, obj2.radius);
}

fn detectCollisions(self: *Game, handle_objects: bool) void {
    var i: usize = 0;
    var bullets = &self.enemy_bullets;
    bullet_loop: while (i < bullets.items.len) {
        const bullet: *const Bullet = &bullets.items[i];
        for (self.players.items) |*player| {
            if (touching(bullet, player.*)) { // hit on player
                _ = bullets.swapRemove(i);
                if (handle_objects) {
                    player.hp -= 1;
                    self.max_enemy_delay += 1;
                }
                continue :bullet_loop;
            }
        }
        i += 1;
    }

    for (self.players.items) |*player| {
        i = 0;
        bullets = &player.bullets;
        bullet_loop: while (i < bullets.items.len) {
            const bullet: *const Bullet = &bullets.items[i];
            var j: usize = 0;
            while (j < self.enemies.items.len) {
                const enemy = &self.enemies.items[j];
                if (touching(bullet, enemy)) { // hit on enemy
                    if (handle_objects) {
                        enemy.hp -= bullet.damage;
                        if (enemy.hp <= 0) {
                            _ = self.enemies.swapRemove(j);
                            player.kills += 1;
                            { // lower player max_delay according to exponential decay
                                player.max_delay -= 0.2;
                                player.max_delay *= 0.95;
                                player.max_delay += 0.2;
                            }
                            self.max_enemy_delay -= 0.1;
                            if (self.max_enemy_delay < self.min_enemy_delay) {
                                self.max_enemy_delay = self.min_enemy_delay;
                            }
                        }
                    }
                    _ = bullets.swapRemove(i);
                    continue :bullet_loop;
                }
                j += 1;
            }
            i += 1;
        }
    }

    if (!handle_objects) return;

    for (self.enemies.items) |enemy| {
        for (self.players.items) |player| {
            if (touching(enemy, player)) { // enemy hit on player (sudden death)
                self.state = .gameover;
                break;
            }
        }
    }
}

fn updateRunningState(self: *Game, frametime: f32) !void {
    for (self.players.items) |*player| {
        try player.updateState(player.player_id == self.player_id, frametime, self.allocator);
    }

    for (self.enemies.items) |*enemy| {
        try enemy.updateState(self, frametime);
    }

    self.detectCollisions(self.connection_manager.connection_info != .client);

    {
        var i: usize = 0;
        var bullets = &self.enemy_bullets;
        while (i < bullets.items.len) {
            var bullet = &bullets.items[i];
            bullet.updateState(frametime);
            if (bullet.range <= 0) {
                _ = bullets.swapRemove(i);
                continue;
            }
            i += 1;
        }

        for (self.players.items) |*player| {
            i = 0;
            bullets = &player.bullets;
            while (i < bullets.items.len) {
                var bullet = &bullets.items[i];
                bullet.updateState(frametime);
                if (bullet.range <= 0) {
                    _ = bullets.swapRemove(i);
                    continue;
                }
                i += 1;
            }
        }
    }

    if (self.connection_manager.connection_info != .client) {
        self.enemy_delay -= frametime;
        if (self.enemy_delay <= 0) {
            try self.addEnemy();
            self.enemy_delay += self.max_enemy_delay;
        }
    }

    for (self.players.items) |player| {
        if (player.hp <= 0) {
            self.state = .gameover;
        }
    }
}

pub fn startLobby(self: *Game, args: anytype) !std.Thread {
    switch (args.connection_type) {
        .client => {
            self.connection_manager.deinit();
            self.connection_manager = try ConnectionManager.init(self.allocator, .client, args);
        },
        .server => {
            self.connection_manager.deinit();
            self.connection_manager = try ConnectionManager.init(self.allocator, .server, args);
        },
        else => unreachable,
    }
    const cm = &self.connection_manager;
    switch (cm.connection_info) {
        .local => unreachable,
        .server => {
            return try std.Thread.spawn(.{}, ConnectionManager.acceptClients, .{ cm, self });
        },
        .client => {
            return try std.Thread.spawn(.{}, ConnectionManager.waitForServer, .{ cm, self });
        },
    }
}

fn new_local(self: *Game) !void {
    self.connection_manager.deinit();
    for (self.players.items[1..]) |*player| {
        player.deinit(self.allocator);
    }
    try self.players.resize(self.allocator, 1);
    Player.resetId();
    const new_id = Player.nextId();
    self.players.items[0].player_id = new_id;
    self.player_id = new_id;
    try self.reinit();
}

pub fn updateState(self: *Game, frametime: f32) !void {
    self.rwlock.lock();
    defer self.rwlock.unlock();

    switch (self.state) {
        .quit => {},
        .menu => {
            try self.menu.updateState(self);
            if (rl.isKeyPressed(rl.KeyboardKey.key_q)) {
                self.state = .quit;
            }
        },
        .gameover => {
            if (self.connection_manager.connection_info != .client) {
                if (rl.isKeyPressed(rl.KeyboardKey.key_enter) or rl.isKeyPressed(rl.KeyboardKey.key_space)) {
                    try self.reinit();
                    self.state = .running;
                }
            }
            if (rl.isKeyPressed(rl.KeyboardKey.key_q)) {
                try self.new_local();
            }
        },
        .disconnected => {
            switch (self.connection_manager.connection_info) {
                .local, .client, .server => {
                    if (rl.isKeyPressed(rl.KeyboardKey.key_enter) or rl.isKeyPressed(rl.KeyboardKey.key_q)) {
                        try self.new_local();
                    }
                },
            }
        },
        .paused => {
            if (self.connection_manager.connection_info != .client) {
                if (rl.isKeyPressed(rl.KeyboardKey.key_escape) or rl.isKeyPressed(rl.KeyboardKey.key_p)) {
                    self.state = .running;
                }
            }
            if (rl.isKeyPressed(rl.KeyboardKey.key_q)) {
                try self.new_local();
            }
            if (rl.isKeyPressed(rl.KeyboardKey.key_v)) {
                self.settings.vsync = !self.settings.vsync;
                self.settings.apply();
            }
            if (rl.isKeyPressed(rl.KeyboardKey.key_f)) rl.toggleFullscreen();
        },
        .running => {
            if (self.connection_manager.connection_info != .client) {
                if (rl.isKeyPressed(rl.KeyboardKey.key_escape) or rl.isKeyPressed(rl.KeyboardKey.key_p)) {
                    self.state = .paused;
                }
            }
            try self.updateRunningState(frametime);
        },
    }
}

fn drawRunning(self: Game) void {
    for (self.enemies.items) |enemy| {
        rl.drawCircleV(enemy.position, enemy.radius, rl.Color.dark_purple);
    }

    for (self.enemy_bullets.items) |bullet| {
        rl.drawCircleV(bullet.position, bullet.radius, bullet.color);
    }

    for (self.players.items) |player| {
        rl.drawCircleV(player.position, player.radius, rl.Color.maroon);
    }
    for (self.players.items) |player| {
        for (player.bullets.items) |bullet| {
            rl.drawCircleV(bullet.position, bullet.radius, bullet.color);
        }
    }
}

fn drawStats(self: Game, player: Player) !void {
    const lives_text = try std.fmt.allocPrintZ(self.allocator, "Lives: {d}", .{player.hp});
    const kills_text = try std.fmt.allocPrintZ(self.allocator, "Kills: {d}", .{player.kills});
    defer {
        self.allocator.free(lives_text);
        self.allocator.free(kills_text);
    }
    rl.drawText(lives_text, 5, 5, 24, rl.Color.sky_blue);
    rl.drawText(kills_text, 5, 30, 24, rl.Color.red);

    switch (self.connection_manager.connection_info) {
        .local => {},
        .client => rl.drawText("client", 5, 55, 24, rl.Color.gold),
        .server => rl.drawText("server", 5, 55, 24, rl.Color.gold),
    }
}

fn drawConnectedPlayers(self: Game) !void {
    for (0.., self.players.items[1..]) |i, player| {
        const playerid_text = try std.fmt.allocPrintZ(self.allocator, "Player {} connected", .{player.player_id});
        defer self.allocator.free(playerid_text);

        rl.drawText(playerid_text, 5, @intCast(5 + i * 25), 24, rl.Color.orange);
    }
}

pub fn draw(self: *Game) !void {
    self.rwlock.lockShared();
    defer self.rwlock.unlockShared();

    const maybe_me = self.getPlayer(self.player_id);
    switch (self.state) {
        .quit => {},
        .menu => {
            if (self.menu.state == .accepting) try self.drawConnectedPlayers();
            self.menu.draw();
        },
        .gameover => {
            self.drawRunning();

            var seed_base64: [11:0]u8 = undefined;
            const b64 = std.base64.standard_no_pad.Encoder;
            _ = b64.encode(&seed_base64, std.mem.asBytes(&self.seed));
            rl.drawText("Game Over", screen_width / 2 - 150, screen_height / 2 - 100, 48, rl.Color.yellow);
            rl.drawText("Seed: " ++ &seed_base64, screen_width / 2 - 150, screen_height / 2, 36, rl.Color.purple);

            if (maybe_me) |me| {
                try self.drawStats(me.*);

                const kills_text = try std.fmt.allocPrintZ(self.allocator, "Kills: {d}", .{me.kills});
                defer self.allocator.free(kills_text);
                rl.drawText(kills_text, screen_width / 2 - 150, screen_height / 2 - 50, 48, rl.Color.red);
            } else {
                rl.drawText("Player not found", 5, 5, 24, rl.Color.red);
            }
        },
        .disconnected => {
            self.drawRunning();
            rl.drawText("Disconnected", screen_width / 2 - 150, screen_height / 2 - 100, 48, rl.Color.red);

            if (maybe_me) |me| {
                try self.drawStats(me.*);
            } else {
                rl.drawText("Player not found", 5, 5, 24, rl.Color.red);
            }
        },
        .paused => {
            self.drawRunning();
            rl.drawText("Paused", screen_width / 2 - 150, screen_height / 2 - 100, 48, rl.Color.blue);

            if (maybe_me) |me| {
                try self.drawStats(me.*);
            } else {
                rl.drawText("Player not found", 5, 5, 24, rl.Color.red);
            }
        },
        .running => {
            self.drawRunning();

            if (maybe_me) |me| {
                try self.drawStats(me.*);
            } else {
                rl.drawText("Player not found", 5, 5, 24, rl.Color.red);
            }
        },
    }
}
