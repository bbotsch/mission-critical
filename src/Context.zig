const Game = @import("Game.zig");
const Menu = @import("Menu.zig");

pub const State = union(enum) {
    game: Game.State,
    menu: Menu.State,
};
