const std = @import("std");
const rl = @import("raylib");
const rmath = @import("raylib-math");
const Bullet = @This();

position: rl.Vector2,
radius: f32 = 5,
speed: rl.Vector2,
range: f32,
damage: f32 = 4,
color: rl.Color,

pub fn updateState(self: *Bullet, frametime: f32) void {
    self.position = rmath.vector2Add(self.position, rmath.vector2Scale(self.speed, frametime));
    self.range -= frametime;
}

pub fn toSerializable(self: Bullet) Bullet {
    return self;
}
