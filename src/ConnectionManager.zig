const std = @import("std");
const net = std.net;
const builtin = @import("builtin");
const network = @import("network");
const s2s = @import("s2s");
const st = @import("serializable_types.zig");
const Context = @import("Game.zig");
const Player = @import("Player.zig");
const ConnectionManager = @This();

const tickrate = 64;

allocator: std.mem.Allocator,
connection_info: union(ConnectionType) {
    local: void,
    server: ConnectionInfo,
    client: ConnectionInfo,
},

pub const ConnectionType = enum {
    local,
    server,
    client,
};

const ConnectionInfo = struct {
    activated: bool = true,
    control: Connections = .{ .tcp = undefined },
    data: Connections = .{ .udp = undefined },
    interval: u64 = std.time.ns_per_s / tickrate,
    threads: [2]std.Thread = undefined,
};

const Connections = union {
    tcp: struct {
        server: net.StreamServer,
        clients: std.ArrayListUnmanaged(net.Stream),
    },
    udp: struct {
        server: network.Socket,
        clients: std.ArrayListUnmanaged(network.EndPoint),
    },
};

pub fn init(allocator: std.mem.Allocator, comptime connection_type: ConnectionType, params: anytype) !ConnectionManager {
    var cm = ConnectionManager{
        .allocator = allocator,
        .connection_info = switch (connection_type) {
            .local => .local,
            .client => .{ .client = .{} },
            .server => .{ .server = .{} },
        },
    };
    if (connection_type == .local) return cm;

    try network.init();
    const addrlist = try net.getAddressList(allocator, params.host, params.port);
    defer addrlist.deinit();

    for (addrlist.addrs) |addr| {
        std.debug.print("Trying address {}\n", .{addr});
        // endpoint is semantically equivalent to addr, but is what the 'network' library needs
        switch (params.connection_type) {
            .server => {
                cm.initServer(addr) catch continue;
                break;
            },
            .client => {
                cm.initClient(addr) catch continue;
                break;
            },
            else => unreachable,
        }
    } else return error.NoSuitableAddressFound;

    return cm;
}

fn initServer(self: *ConnectionManager, addr: net.Address) !void {
    const endpoint = try network.EndPoint.fromSocketAddress(&addr.any, addr.getOsSockLen());

    var stream_server = net.StreamServer.init(.{
        .force_nonblocking = true,
        .reuse_address = true,
    });
    try stream_server.listen(addr);
    errdefer stream_server.close();

    std.debug.print("Binding UDP server\n", .{});
    var data_server = try network.Socket.create(endpoint.address, .udp);
    errdefer data_server.close();

    try data_server.setReadTimeout(std.time.us_per_s);
    try data_server.bind(endpoint);

    const ci = &self.connection_info.server;
    ci.control.tcp = .{
        .server = stream_server,
        .clients = try std.ArrayListUnmanaged(net.Stream).initCapacity(self.allocator, 1),
    };
    ci.data.udp = .{
        .server = data_server,
        .clients = try std.ArrayListUnmanaged(network.EndPoint).initCapacity(self.allocator, 1),
    };

    // player 1 doesn't need client connections
    _ = ci.control.tcp.clients.addOneAssumeCapacity();
    _ = ci.data.udp.clients.addOneAssumeCapacity();

    std.debug.print("Listening on address {}\n", .{addr});
}

fn initClient(self: *ConnectionManager, addr: net.Address) !void {
    const endpoint = try network.EndPoint.fromSocketAddress(&addr.any, addr.getOsSockLen());

    const stream = try net.tcpConnectToAddress(addr);
    errdefer stream.close();

    var data_server = try network.Socket.create(endpoint.address, .udp);
    errdefer data_server.close();

    try data_server.setReadTimeout(std.time.us_per_s);
    try data_server.connect(endpoint);

    var control_streams = try std.ArrayListUnmanaged(net.Stream).initCapacity(self.allocator, 1);
    control_streams.appendAssumeCapacity(stream);

    const ci = &self.connection_info.client;
    ci.control.tcp = .{
        .server = undefined,
        .clients = control_streams,
    };
    ci.data.udp = .{
        .server = data_server,
        .clients = undefined,
    };

    std.debug.print("Connected to address {}\n", .{addr});
}

pub fn deinit(self: *ConnectionManager) void {
    self.stop();
    switch (self.connection_info) {
        .local => {},
        .server => |*ci| {
            for (ci.control.tcp.clients.items[1..]) |stream| {
                stream.close();
            }
            ci.control.tcp.server.deinit();
            ci.data.udp.server.close();

            ci.control.tcp.clients.deinit(self.allocator);
            ci.data.udp.clients.deinit(self.allocator);
        },
        .client => |*ci| {
            ci.control.tcp.clients.items[0].close();
            ci.data.udp.server.close();

            ci.control.tcp.clients.deinit(self.allocator);
        },
    }
    network.deinit();
    self.connection_info = .local;
}

pub fn setupClient(self: *ConnectionManager, ctx: *Context) !void {
    switch (self.connection_info) {
        .local => return,
        .client => |ci| {
            const socket = ci.data.udp.server;
            errdefer socket.close();

            _ = try socket.send("mission-critical"); // client hello
            _ = try ci.data.udp.server.receive(std.mem.asBytes(&ctx.player_id));

            return;
        },
        .server => |*ci| {
            const conn = try ci.control.tcp.server.accept();
            try ci.control.tcp.clients.append(self.allocator, conn.stream);

            const socket = ci.data.udp.server;
            var msg: [16]u8 = undefined;
            const receive_from = try socket.receiveFrom(&msg); // client hello
            if ((receive_from.numberOfBytes != 16) or (!std.mem.eql(u8, &msg, "mission-critical"))) return error.ClientHelloFailed;
            const endpoint = receive_from.sender;

            const player = try Player.init(ctx.allocator, null);
            try ctx.players.append(ctx.allocator, player);

            _ = try socket.sendTo(endpoint, std.mem.asBytes(&player.player_id)); // server responds with player's index
            try ci.data.udp.clients.append(self.allocator, endpoint);

            std.debug.print("Client connected: {}\n", .{endpoint});
        },
    }
}

pub fn acceptClients(self: *ConnectionManager, ctx: *Context) !void {
    const ci = &self.connection_info.server;

    std.debug.print("Waiting for clients to connect...\n", .{});
    while (ctx.menu.state == .accepting) {
        self.setupClient(ctx) catch |err| switch (err) {
            error.WouldBlock => std.time.sleep(200 * std.time.ns_per_ms),
            else => return err,
        };
    }
    ci.control.tcp.server.close();

    // Set game enemy_delay according to number of players
    const num_players = ctx.players.items.len;
    ctx.min_enemy_delay /= @as(f32, @floatFromInt(num_players + 1)) / 2;
    ctx.max_enemy_delay /= @as(f32, @floatFromInt(num_players + 1)) / 2;

    for (ci.control.tcp.clients.items[1..]) |client| {
        try client.writeAll(&[1]u8{0}); // send signal to start running game
    }
    try self.start(ctx);
}

pub fn waitForServer(self: *ConnectionManager, ctx: *Context) !void {
    try self.setupClient(ctx);

    const ci = self.connection_info.client;
    var msg: [1]u8 = undefined;
    _ = try ci.control.tcp.clients.items[0].readAll(&msg); // wait for server to send signal to start running game
    try self.start(ctx);

    ctx.menu.state = .main;
}

pub fn start(self: *ConnectionManager, ctx: *Context) !void {
    switch (self.connection_info) {
        .local => {},
        .server => {
            try self.startServerThreads(ctx);
        },
        .client => {
            try self.startClientThreads(ctx);
        },
    }
}

fn stop(self: *ConnectionManager) void {
    switch (self.connection_info) {
        .local => {},
        .server, .client => |*ci| {
            ci.activated = false;
            ci.threads[0].join();
            ci.threads[1].join();
        },
    }
}

fn startServerThreads(self: *ConnectionManager, ctx: *Context) !void {
    const ci = &self.connection_info.server;
    ci.threads[0] = try std.Thread.spawn(.{}, serverPushState, .{ self, ctx });
    ci.threads[1] = try std.Thread.spawn(.{}, serverPullState, .{ self, ctx });
}

fn startClientThreads(self: *ConnectionManager, ctx: *Context) !void {
    const ci = &self.connection_info.client;
    ci.threads[0] = try std.Thread.spawn(.{}, clientPushState, .{ self, ctx });
    ci.threads[1] = try std.Thread.spawn(.{}, clientPullState, .{ self, ctx });
}

fn serverPushState(self: *ConnectionManager, ctx: *Context) !void {
    const ci = &self.connection_info.server;

    var socket = ci.data.udp.server;
    var state: st.SerializableGame = undefined;
    state.players.items = try self.allocator.alloc(st.SerializablePlayer, ctx.players.items.len);
    for (state.players.items) |*player| {
        player.bullets.items = try self.allocator.alloc(st.SerializableBullet, 0);
    }
    const enemies = &state.enemies.items;
    const enemy_bullets = &state.enemy_bullets.items;
    enemies.* = try self.allocator.alloc(st.SerializableEnemy, 0);
    enemy_bullets.* = try self.allocator.alloc(st.SerializableBullet, 0);
    defer {
        self.allocator.free(enemies.*);
        self.allocator.free(enemy_bullets.*);
        for (state.players.items) |*player| {
            self.allocator.free(player.bullets.items);
        }
        self.allocator.free(state.players.items);
    }

    while (ci.activated) : (std.time.sleep(ci.interval)) {
        {
            ctx.rwlock.lockShared();
            defer ctx.rwlock.unlockShared();

            for (state.players.items, ctx.players.items) |*state_player, ctx_player| {
                const bullets = &state_player.bullets.items;
                bullets.* = try self.allocator.realloc(bullets.*, ctx_player.bullets.items.len);
            }

            enemies.* = try self.allocator.realloc(enemies.*, ctx.enemies.items.len);
            enemy_bullets.* = try self.allocator.realloc(enemy_bullets.*, ctx.enemy_bullets.items.len);
            st.copyRecursive(st.SerializableGame, &state, ctx.*);
        }

        for (ci.data.udp.clients.items[1..], state.players.items[1..]) |endpoint, player| {
            socket.endpoint = endpoint;
            var buffered_socket = std.io.bufferedWriter(socket.writer());

            state.player_id = player.player_id;

            // This could be a std.io.MultiWriter, but would be tricky to orchestrate
            // Should probably just send player_id separately instead of as part of the game state
            // Or just update player_id using control stream once and then just use that
            s2s.serialize(buffered_socket.writer(), st.SerializableGame, state) catch |err| {
                std.debug.print("Error serializing server state: {}\n", .{err});
                return err;
            };
            buffered_socket.flush() catch |err| {
                std.debug.print("Error writing to UDP connection for player_id {}: {}\n", .{ player.player_id, err });
                // TODO: Remove player from ctx.players and connections instead of going into disconnected state
                ci.activated = false;
                ctx.state = .disconnected;
            };
        }
    }
}

fn clientPullState(self: *ConnectionManager, ctx: *Context) !void {
    const ci = &self.connection_info.client;

    const socket = ci.data.udp.server;
    var buffered_socket = std.io.bufferedReader(socket.reader());
    const reader = buffered_socket.reader();
    var state: st.SerializableGame = undefined;

    while (ci.activated) {
        state = s2s.deserializeAlloc(reader, st.SerializableGame, self.allocator) catch |err| switch (err) {
            error.WouldBlock, error.ConnectionRefused => {
                ci.activated = false;
                ctx.state = .disconnected;
                break;
            },
            else => return err,
        };
        defer s2s.free(self.allocator, st.SerializableGame, &state);

        // I don't like this, how can i refactor this?
        if (!ci.activated) break;

        ctx.rwlock.lock();
        defer ctx.rwlock.unlock();

        ctx.seed = state.seed;
        ctx.player_id = state.player_id;
        ctx.state = state.state;

        try ctx.enemies.resize(ctx.allocator, state.enemies.items.len);
        st.copyRecursive(st.SerializableArrayList(st.SerializableEnemy), &ctx.enemies, state.enemies);

        try ctx.enemy_bullets.resize(ctx.allocator, state.enemy_bullets.items.len);
        st.copyRecursive(st.SerializableArrayList(st.SerializableBullet), &ctx.enemy_bullets, state.enemy_bullets);

        const old_len = ctx.players.items.len;
        if (state.players.items.len < old_len) {
            for (ctx.players.items[state.players.items.len..]) |*player| {
                player.deinit(ctx.allocator);
            }
        }
        try ctx.players.resize(ctx.allocator, state.players.items.len);
        if (old_len < ctx.players.items.len) {
            for (ctx.players.items[old_len..]) |*player| {
                player.* = try Player.init(ctx.allocator, 0);
            }
        }
        for (ctx.players.items, state.players.items) |*ctx_player, state_player| {
            if (state_player.player_id != state.player_id) {
                try ctx_player.bullets.resize(ctx.allocator, state_player.bullets.items.len);
                st.copyRecursive(st.SerializablePlayer, ctx_player, state_player);
            } else {
                st.copyRecursive(st.PlayerStats, ctx_player, state_player);
            }
        }
    }
}

fn clientPushState(self: *ConnectionManager, ctx: *Context) !void {
    const ci = &self.connection_info.client;

    const socket = ci.data.udp.server;
    var buffered_socket = std.io.bufferedWriter(socket.writer());
    const writer = buffered_socket.writer();
    var state: st.SerializablePlayerPartial = undefined;
    state.bullets.items = try self.allocator.alloc(st.SerializableBullet, 0);
    defer self.allocator.free(state.bullets.items);

    while (ci.activated) : (std.time.sleep(ci.interval)) {
        {
            ctx.rwlock.lockShared();
            defer ctx.rwlock.unlockShared();

            const player = ctx.getPlayer(ctx.player_id) orelse continue;

            state.bullets.items = try self.allocator.realloc(state.bullets.items, player.bullets.items.len);

            st.copyRecursive(st.SerializablePlayerPartial, &state, player);
        }

        s2s.serialize(writer, st.SerializablePlayerPartial, state) catch |err| {
            std.debug.print("Error serializing client data: {}\n", .{err});
            return err;
        };
        buffered_socket.flush() catch |err| {
            std.debug.print("Error writing to UDP connection: {}\n", .{err});
            ci.activated = false;
            ctx.state = .disconnected;
        };
    }
}

fn serverPullState(self: *ConnectionManager, ctx: *Context) !void {
    const ci = &self.connection_info.server;

    var state: st.SerializablePlayerPartial = undefined;
    const socket = ci.data.udp.server;
    var buffered_socket = std.io.bufferedReader(socket.reader());

    while (ci.activated) {
        state = s2s.deserializeAlloc(buffered_socket.reader(), st.SerializablePlayerPartial, self.allocator) catch |err| switch (err) {
            error.WouldBlock => {
                ci.activated = false;
                ctx.state = .disconnected;
                break;
            },
            else => return err,
        };
        defer s2s.free(self.allocator, st.SerializablePlayerPartial, &state);

        // I don't like this, how can i refactor this?
        if (!ci.activated) break;

        ctx.rwlock.lock();
        defer ctx.rwlock.unlock();

        const player = ctx.getPlayer(state.player_id) orelse continue; //return error.InvalidPlayerId;
        try player.bullets.resize(ctx.allocator, state.bullets.items.len);
        st.copyRecursive(st.SerializablePlayerPartial, player, state);
    }
}
