const std = @import("std");
const rl = @import("raylib");
const Game = @import("Game.zig");

// TODO: use type reflection to automatically grab types of fields from their respective types automatically

pub fn SerializableArrayList(comptime T: type) type {
    return struct {
        items: []T,
    };
}

pub const SerializableGame = struct {
    seed: u64,
    players: SerializableArrayList(SerializablePlayer),
    player_id: u8,
    enemies: SerializableArrayList(SerializableEnemy),
    enemy_bullets: SerializableArrayList(SerializableBullet),
    state: Game.State,
};

pub const SerializablePlayer = struct {
    player_id: u8,
    position: rl.Vector2,
    speed: rl.Vector2,
    max_speed: f32,
    accel: rl.Vector2,
    max_accel: f32,
    friction: f32,
    radius: f32,
    max_delay: f32,
    bullets: SerializableArrayList(SerializableBullet),
    hp: i8,
    kills: u16,
};

pub const SerializablePlayerPartial = struct {
    player_id: u8,
    position: rl.Vector2,
    speed: rl.Vector2,
    max_speed: f32,
    accel: rl.Vector2,
    max_accel: f32,
    friction: f32,
    radius: f32,
    bullets: SerializableArrayList(SerializableBullet),
};

pub const PlayerStats = struct {
    player_id: u8,
    max_delay: f32,
    hp: i8,
    kills: u16,
};

pub const SerializableEnemy = struct {
    position: rl.Vector2,
    speed: rl.Vector2,
    max_speed: f32,
    accel: rl.Vector2,
    max_accel: f32,
    friction: f32,
    radius: f32,
    hp: f32,
};

pub const SerializableBullet = @import("Bullet.zig");

// const Rule = enum {
//     copy,
//     ref,
//     noop,
// };
// const CopyRules = .{
//     .position = Rule.copy,
//     .speed = Rule.copy,
//     .bullets = .{ .len = Rule.copy, .items = Rule.copy },
//     .players = .{ .buffer = [2]Rule{ Rule.noop, Rule.copy } },
// };

/// 'to' and 'from' must be types that can be converted into type T loosely speaking
pub fn copyRecursive(comptime T: type, to: anytype, from: anytype) void {
    switch (@typeInfo(T)) {
        .Struct => |sinfo| inline for (sinfo.fields) |field|
            copyRecursive(field.type, &@field(to, field.name), @field(from, field.name)),
        .Array => |ainfo| {
            // for (0.., from) |i, from_elem| {
            //     copyRecursive(ainfo.child, &to.*[i], from_elem);
            // }
            for (to, from) |*to_elem, from_elem| {
                copyRecursive(ainfo.child, to_elem, from_elem);
            }
        },
        .Pointer => |pinfo| switch (pinfo.size) {
            .One => copyRecursive(pinfo.child, to.*, from.*),
            .Many => {
                if (pinfo.sentinel) |sentinel_raw| {
                    const sentinel: pinfo.child = @as(*const pinfo.child, @ptrCast(sentinel_raw)).*;
                    var i: usize = 0;
                    while (from[i] != sentinel) : (i += 1) {
                        copyRecursive(pinfo.child, &to.*[i], from[i]);
                    }
                    to.*[i] = sentinel;
                } else @compileError("Non sentinel-terminated many-item pointers not supported");
            },
            .Slice => {
                for (to.*, from) |*to_elem, from_elem|
                    copyRecursive(pinfo.child, to_elem, from_elem);
            },
            .C => @compileError("C pointers not supported"),
        },
        .Int => {
            switch (@typeInfo(@TypeOf(from))) {
                .Int => {
                    to.* = @intCast(from);
                },
                .Float => {
                    to.* = @intFromFloat(from);
                },
                .Pointer => copyRecursive(T, to, from.*),
                else => @compileError("Type incompatible with integer"),
            }
        },
        .Float => {
            switch (@typeInfo(@TypeOf(from))) {
                .Float => {
                    to.* = @floatCast(from);
                },
                .Int => {
                    to.* = @floatFromInt(from);
                },
                .Pointer => copyRecursive(T, to, from.*),
                else => @compileError("Type incompatible with float"),
            }
        },
        else => {
            to.* = from;
        },
    }
}

pub fn copyRecursiveNoDeref(comptime T: type, to: anytype, from: anytype) void {
    switch (@typeInfo(T)) {
        .Struct => |sinfo| inline for (sinfo.fields) |field|
            copyRecursiveNoDeref(field.type, &@field(to, field.name), @field(from, field.name)),
        .Array => |ainfo| for (to, from) |*to_elem, from_elem| {
            copyRecursiveNoDeref(ainfo.child, to_elem, from_elem);
        },
        .Pointer => {
            to.* = from;
        },
        .Int => {
            switch (@typeInfo(@TypeOf(from))) {
                .Int => {
                    to.* = @intCast(from);
                },
                .Float => {
                    to.* = @intFromFloat(from);
                },
                .Pointer => copyRecursiveNoDeref(T, to, from.*),
                else => @compileError("Type incompatible with integer"),
            }
        },
        .Float => {
            switch (@typeInfo(@TypeOf(from))) {
                .Float => {
                    to.* = @floatCast(from);
                },
                .Int => {
                    to.* = @floatFromInt(from);
                },
                .Pointer => copyRecursiveNoDeref(T, to, from.*),
                else => @compileError("Type incompatible with float"),
            }
        },
        else => {
            to.* = from;
        },
    }
}
