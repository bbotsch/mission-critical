const std = @import("std");
const rl = @import("raylib");
const rmath = @import("raylib-math");
const Context = @import("Game.zig");
const Bullet = @import("Bullet.zig");
const Player = @This();

const multiplier = 1.5 * 60;

var next_id: u8 = 0;
pub fn nextId() u8 { // pub because of edge case in Game.new_locl()
    next_id += 1;
    return next_id;
}
pub fn resetId() void {
    next_id = 0;
}

player_id: u8,
position: rl.Vector2 = rl.Vector2.init(0, 0),
speed: rl.Vector2 = rl.Vector2.init(0, 0),
max_speed: f32 = 5 * multiplier,
accel: rl.Vector2 = rl.Vector2.init(0, 0),
max_accel: f32 = 40 * multiplier,
friction: f32 = 30 * multiplier,
radius: f32 = 30,
bullet_delay: f32 = 0,
max_delay: f32 = 1,
shotspeed: f32 = 3 * multiplier,
range: f32 = 1,
bullets: std.ArrayListUnmanaged(Bullet) = undefined,
hp: i8 = 3,
kills: u16 = 0,

pub fn init(allocator: std.mem.Allocator, player_id: ?u8) !Player {
    return .{
        .player_id = if (player_id) |id| id else nextId(),
        .bullets = try std.ArrayListUnmanaged(Bullet).initCapacity(allocator, 0),
    };
}

pub fn deinit(self: *Player, allocator: std.mem.Allocator) void {
    self.bullets.deinit(allocator);
}

fn addBullet(self: *Player, bullet_speed: rl.Vector2, allocator: std.mem.Allocator) !void {
    const speed_modifier = rmath.vector2Scale(self.speed, 0.3);
    try self.bullets.append(allocator, .{
        .position = self.position,
        .speed = rmath.vector2Add(bullet_speed, speed_modifier),
        .range = self.range,
        .color = rl.Color.green,
    });
}

pub fn updateState(self: *Player, handle_controls: bool, frametime: f32, allocator: std.mem.Allocator) !void {
    if (handle_controls) {
        self.accel = rmath.vector2Zero();
        if (rl.isKeyDown(rl.KeyboardKey.key_d)) {
            self.accel.x += self.max_accel;
        }
        if (rl.isKeyDown(rl.KeyboardKey.key_a)) {
            self.accel.x -= self.max_accel;
        }
        if (rl.isKeyDown(rl.KeyboardKey.key_w)) {
            self.accel.y -= self.max_accel;
        }
        if (rl.isKeyDown(rl.KeyboardKey.key_s)) {
            self.accel.y += self.max_accel;
        }
        self.accel = rmath.vector2ClampValue(self.accel, 0, self.max_accel);
        const override_friction = rmath.vector2Scale(rmath.vector2Normalize(self.accel), self.friction);
        self.accel = rmath.vector2Add(self.accel, override_friction);
    }

    const fric = rmath.vector2Scale(rmath.vector2Normalize(self.speed), -self.friction);
    const new_speed = rmath.vector2Add(self.speed, rmath.vector2Scale(fric, frametime));
    self.speed.x = if (self.speed.x * new_speed.x > 0) new_speed.x else 0;
    self.speed.y = if (self.speed.y * new_speed.y > 0) new_speed.y else 0;

    self.speed = rmath.vector2Add(self.speed, rmath.vector2Scale(self.accel, frametime));
    self.speed = rmath.vector2ClampValue(self.speed, 0, self.max_speed);

    self.position = rmath.vector2Add(self.position, rmath.vector2Scale(self.speed, frametime));

    const min_position = rl.Vector2.init(self.radius, self.radius);
    const max_position = rl.Vector2.init(
        Context.screen_width - self.radius,
        Context.screen_height - self.radius,
    );

    if (self.position.x < min_position.x) {
        self.position.x = min_position.x;
        self.speed.x = 0;
    } else if (self.position.x > max_position.x) {
        self.position.x = max_position.x;
        self.speed.x = 0;
    }

    if (self.position.y < min_position.y) {
        self.position.y = min_position.y;
        self.speed.y = 0;
    } else if (self.position.y > max_position.y) {
        self.position.y = max_position.y;
        self.speed.y = 0;
    }

    if (handle_controls) {
        self.bullet_delay -= frametime;
        if (self.bullet_delay <= 0) {
            const bullet_speed = if (rl.isKeyDown(rl.KeyboardKey.key_right))
                rl.Vector2.init(self.shotspeed, 0)
            else if (rl.isKeyDown(rl.KeyboardKey.key_left))
                rl.Vector2.init(-self.shotspeed, 0)
            else if (rl.isKeyDown(rl.KeyboardKey.key_up))
                rl.Vector2.init(0, -self.shotspeed)
            else if (rl.isKeyDown(rl.KeyboardKey.key_down))
                rl.Vector2.init(0, self.shotspeed)
            else
                null;

            if (bullet_speed) |bs| {
                try self.addBullet(bs, allocator);
                self.bullet_delay += self.max_delay;
            } else {
                self.bullet_delay += frametime;
            }
        }
    }
}
